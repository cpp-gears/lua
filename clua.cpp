#include "clua.h"
#include <memory>
#include <ctime>
#include <set>
#include <map>
#include <list>
#include <deque>
#include <cstring>
#include <cstdarg>

#define L luaObject.get()

using namespace lib;

namespace lua {
    namespace typecast {
        void any(lua_State* l, std::any& val);
    }
}

clua::clua(const std::string& scriptname, const std::string& progdir)
    : luaScript{ scriptname }, luaPwd{ progdir }
{
    if (luaPwd.empty()) {
        auto lastBackSlash{ luaScript.find_last_of('/') };
        luaPwd.assign(luaScript.begin(), lastBackSlash != std::string::npos ? (luaScript.begin() + lastBackSlash + 1) : luaScript.begin());
    }
}

clua::~clua() {

}

const std::string& clua::script(const std::string_view& scriptname, const std::string_view& progdir) {
    luaScript = scriptname;
    if (!progdir.empty()) { luaPwd = progdir; }
    if (luaPwd.empty()) {
        auto lastBackSlash{ luaScript.find_last_of('/') };
        luaPwd.assign(luaScript.begin(), lastBackSlash != std::string::npos ? (luaScript.begin() + lastBackSlash + 1) : luaScript.begin());
    }
    return luaScript;
}

void clua::cargs::get(const std::string& type, ...) const {
    va_list lst;
    va_start(lst, type);
    int nidx = (int)luaStackOffset;
    auto n = lua_gettop(luaState);
    for (const char* f{ type.data() }; *f && nidx < n /* <= */; f++, nidx++) {
        int type = lua_type(luaState, nidx);
        switch (*f) {
        case 'r':
            if (type == LUA_TSTRING) {
                size_t len;
                auto str{ (const uint8_t*)lua_tolstring(luaState, nidx, &len) };
                if (len) {
                    va_arg(lst, std::vector<uint8_t>*)->assign(str, str + len);
                }
            }
            break;
        case 's':
            if (type == LUA_TSTRING) {
                va_arg(lst, std::string*)->assign(luaL_checkstring(luaState, nidx));
            }
            else {
                va_arg(lst, std::string*)->assign({});
            }
            break;
        case 'b':
            if (type == LUA_TBOOLEAN) {
                *va_arg(lst, bool*) = (bool)lua_toboolean(luaState, nidx);
            }
            break;
        case 'i':
            *va_arg(lst, ssize_t*) = (ssize_t)luaL_checkinteger(luaState, nidx);
            //*va_arg(lst, ssize_t*)
            break;
        case 't':
            if (type == LUA_TTABLE) {
                /* table unordered_map<std::string,std::string> */
                if (auto& map = *(std::unordered_map<std::string, std::string>*)(va_arg(lst, ssize_t*)); lua_istable(luaState, nidx)) {
                    //lua_settop(luaState, nidx);
                    lua_pushnil(luaState);
                    while (lua_next(luaState, nidx)) { // -2, because we have table at -1
                        if (lua_isnumber(luaState, -2)) {
                            map.emplace(std::to_string(lua_tointeger(luaState, -2)), lua_tostring(luaState, -1));
                        }
                        else if (lua_isstring(luaState, -2)) { // only store stuff with string keys
                            map.emplace(lua_tostring(luaState, -2), lua_tostring(luaState, -1));
                        }
                        lua_pop(luaState, 1); // remove value, keep key for lua_next    
                    }
                    //lua_settop(luaState, nidx);
                }
            }
            break;
        }
    }
    va_end(lst);

}

ssize_t clua::execute(const std::string& functionname, const std::vector<std::any>&& args, int nresults, std::vector<std::any>&& results) {
    std::shared_ptr<lua_State>luaObject{ luaL_newstate(), [](lua_State* l) {
        lua_close(l); 
    } };
    try {
        luaL_openlibs(L);

        for (auto&& [mname, mfn] : luaModules) {
            __module(L, mname.c_str(), mfn.cfunctions);
        }

        *reinterpret_cast<clua**>(lua_newuserdata(L, sizeof(clua*))) = this;
        lua_setglobal(L, "$CLUATHIS");

        /* adjust lua package.path */
        {
            lua_getglobal(L, "package");
            lua_getfield(L, -1, "path"); // get field "path" from table at top of stack (-1)
            std::string cur_path = lua_tostring(L, -1); // grab path string from top of stack
            cur_path.append(";").append(luaPwd).append("?.lua");
            cur_path.append(";").append(luaPwd).append("?");
            lua_pop(L, 1); // get rid of the string on the stack we just pushed on line 5
            lua_pushstring(L, cur_path.c_str()); // push the new one
            lua_setfield(L, -2, "path"); // set the field "path" in table at -2 with value at top of stack
            lua_pop(L, 1); // get rid of package table from top of stack
        }

        if (luaL_dofile(L, luaScript.c_str()) == 0) {
            lua_getglobal(L, functionname.c_str());
            if (/*auto nfn = lua_gettop(L); */lua_isfunction(L, -1)) {

                for (auto v : args) { lua::typecast::any(L, v); }

                //printf("LUA::CALL.%s\n", function.c_str());
                if (lua_pcall(L, (int)args.size(), (int)nresults, 0) == 0) {
                    if (nresults) {
                        results.reserve(nresults);
                        for (; nresults; --nresults) {
                            if (lua_isinteger(L, -nresults)) {
                                results.emplace_back(std::any{ ssize_t(lua_tointeger(L, -nresults)) });
                            }
                            else if (lua_isboolean(L, -nresults)) {
                                results.emplace_back(std::any{ bool(lua_toboolean(L, -nresults)) });
                            }
                            else if (lua_isnil(L, -nresults)) {
                                results.emplace_back(std::any{ nullptr });
                            }
                            else if (lua_isstring(L, -nresults)) {
                                size_t ptr_len{ 0 };
                                if (auto ptr_str = lua_tolstring(L, -nresults, &ptr_len); ptr_str && ptr_len) {
                                    results.emplace_back(std::string{ ptr_str,ptr_str + ptr_len });
                                }
                                else {
                                    results.emplace_back(std::string{ });
                                }
                            }
                            else if (lua_istable(L, -nresults)) {
                                std::unordered_map<std::string, std::string> map;
                                //
                                //lua_gettable(L,-nresults);
                                auto ntop = lua_gettop(L);
                                lua_pushnil(L);
                                while (lua_next(L, -nresults - 1)) { // -2, because we have table at -1
                                    if (lua_isnumber(L, -2)) {
                                        map.emplace(std::to_string(lua_tointeger(L, -2)), lua_tostring(L, -1));
                                    }
                                    else if (lua_isstring(L, -2)) { // only store stuff with string keys
                                        map.emplace(lua_tostring(L, -2), lua_tostring(L, -1));
                                    }
                                    lua_pop(L, 1); // remove value, keep key for lua_next    
                                }
                                results.emplace_back(map);
                                lua_settop(L, ntop);
                            }
                        }
                    }
                }
                else {
                    fprintf(stderr, "clua::execute(error running function `%s`): %s\n", functionname.c_str(), lua_tostring(L, -1));
                }
            }
            else {
                fprintf(stderr, "clua::execute(error running function `%s`): %s\n", functionname.c_str(), lua_tostring(L, -1));
            }
            lua_pop(L, 1);
        }
        else {
            fprintf(stderr, "clua::execute(error running function `%s`): %s\n", functionname.c_str(), lua_tostring(L, -1));
            return -ENOEXEC;
        }
    }
    catch (std::exception& e) {
        fprintf(stderr, "clua::execute(std::exception throw): %s\n", e.what());
        return -ENOEXEC;
    }
    catch (...) {
        fprintf(stderr, "clua::execute(exception)\n");
        return -ENOEXEC;
    }

    return 0;
}

int clua::__call(lua_State* l, const char* function, const clua::cargs& args) {
    if (auto&& it = luaMethods.find(function); it != luaMethods.end()) {
        auto&& result = it->second(args);
        for (auto v : result) {
            lua::typecast::any(l, v);
        }
        return (int)result.size();
    }
    return 0;
}

int clua::call(lua_State* l) {
    if (lua_Debug entry; lua_getstack(l, 0, &entry) && lua_getinfo(l, "n", &entry))
    {
        lua_getglobal(l, "$CLUATHIS");
        if (auto pThis = (clua**)lua_touserdata(l, -1); pThis && *pThis) {
            lua_gettop(l);
            return (*pThis)->__call(l, entry.name, { l,1 });
        }
    }
    return 0;
}

void clua::module(const std::string& modulename, std::unordered_map<std::string, function> functions) {

    auto&& it = luaModules.emplace(modulename, module_t{ .name = modulename });

    it.first->second.cfunctions.clear();
    it.first->second.cfunctions.reserve(functions.size() + 1);

    for (auto&& [name, fn] : functions) {
        auto&& mit = luaMethods.emplace(name, fn);
        it.first->second.cfunctions.push_back(cfunction{ mit.first->first.c_str(),clua::call });
    }
    it.first->second.cfunctions.push_back({ nullptr, nullptr });
}

void clua::__module(lua_State* l, const char* name, std::vector<struct luaL_Reg>& fns) {
    luaL_newmetatable(l, name);
    luaL_setfuncs(l, fns.data(), 0);
    lua_pushvalue(l, -1);
    lua_setglobal(l, name);
}

namespace lua {
    namespace typecast {

        void any(lua_State* l, std::any& val);

        template<typename T>
        void push_integer(lua_State* l, std::any& val) { lua_pushinteger(l, std::any_cast<T>(val)); }
        template<typename T>
        void push_number(lua_State* l, std::any& val) { lua_pushnumber(l, std::any_cast<T>(val)); }
        void push_bool(lua_State* l, std::any& val) { lua_pushboolean(l, std::any_cast<bool>(val)); }
        void push_string(lua_State* l, std::any& val) { lua_pushstring(l, std::any_cast<const char*>(val)); }
        void push_cstring(lua_State* l, std::any& val) { auto&& _v = std::any_cast<std::string>(val); lua_pushlstring(l, _v.data(), _v.length()); }
        void push_rawdata(lua_State* l, std::any& val) { auto&& _v = std::any_cast<std::vector<uint8_t>>(val); lua_pushlstring(l, (const char*)_v.data(), _v.size()); }
        void push_nil(lua_State* l, std::any& val) { lua_pushnil(l); }
        void push_umapss(lua_State* l, std::any& val) {
            lua_newtable(l);
            if (!std::any_cast<std::unordered_map<std::string, std::string>>(val).empty()) {
                for (auto&& [k, v] : std::any_cast<std::unordered_map<std::string, std::string>>(val)) {
                    lua_pushlstring(l, k.c_str(), k.length());
                    lua_pushlstring(l, v.c_str(), v.length());
                    lua_settable(l, -3);
                }
            }
        }
        void push_mapss(lua_State* l, std::any& val) {
            lua_newtable(l);
            if (!std::any_cast<std::map<std::string, std::string>>(val).empty()) {
                for (auto&& [k, v] : std::any_cast<std::map<std::string, std::string>>(val)) {
                    lua_pushlstring(l, k.c_str(), k.length());
                    lua_pushlstring(l, v.c_str(), v.length());
                    lua_settable(l, -3);
                }
            }
        }
        void push_mapsa(lua_State* l, std::any& val) {
            lua_newtable(l);
            if (!std::any_cast<std::unordered_map<std::string, std::any>>(val).empty()) {
                for (auto&& [k, v] : std::any_cast<std::unordered_map<std::string, std::any>>(val)) {
                    lua_pushlstring(l, k.c_str(), k.length());
                    lua::typecast::any(l, v);
                    lua_settable(l, -3);
                }
            }
        }

        void push_smapsa(lua_State* l, std::any& val) {
            lua_newtable(l);
            if (!std::any_cast<std::map<std::string, std::any>>(val).empty()) {
                for (auto&& [k, v] : std::any_cast<std::map<std::string, std::any>>(val)) {
                    lua_pushlstring(l, k.c_str(), k.length());
                    lua::typecast::any(l, v);
                    lua_settable(l, -3);
                }
            }
        }

        void push_mapssa(lua_State* l, std::any& val) {
            lua_newtable(l);
            if (!std::any_cast<std::map<std::string, std::unordered_map<std::string, std::any>>>(val).empty()) {
                for (auto&& [k, v] : std::any_cast<std::map<std::string, std::unordered_map<std::string, std::any>>>(val)) {
                    lua_pushlstring(l, k.c_str(), k.length());
                    lua_newtable(l);
                    for (auto&& opt : v) {
                        lua_pushlstring(l, opt.first.c_str(), opt.first.length());
                        lua::typecast::any(l, opt.second);
                        lua_settable(l, -3);
                    }
                    lua_settable(l, -3);
                }
            }
        }

        void push_sets(lua_State* l, std::any& val) {
            lua_newtable(l);
            if (!std::any_cast<std::set<std::string>>(val).empty()) {
                int n = 1;
                for (auto&& v : std::any_cast<std::set<std::string>>(val)) {
                    lua_pushinteger(l, n++);
                    lua_pushlstring(l, v.c_str(), v.length());
                    lua_settable(l, -3);
                }
            }
        }

        void push_seta(lua_State* l, std::any& val) {
            lua_newtable(l);
            if (!std::any_cast<std::set<std::any>>(val).empty()) {
                int n = 1;
                for (auto&& v : std::any_cast<std::set<std::any>>(val)) {
                    lua_pushinteger(l, n++);
                    lua::typecast::any(l, (std::any&)v);
                    lua_settable(l, -3);
                }
            }
        }
        template<typename T>
        void push_seti(lua_State* l, std::any& val) {
            lua_newtable(l);
            if (!std::any_cast<std::set<T>>(val).empty()) {
                int n = 1;
                for (auto&& v : std::any_cast<std::set<T>>(val)) {
                    lua_pushinteger(l, n++);
                    lua_pushinteger(l, v);
                    lua_settable(l, -3);
                }
            }
        }

        void push_deqa(lua_State* l, std::any& val) {
            lua_newtable(l);
            if (!std::any_cast<std::deque<std::any>>(val).empty()) {
                int n = 1;
                for (auto&& v : std::any_cast<std::deque<std::any>>(val)) {
                    lua_pushinteger(l, n++);
                    lua::typecast::any(l, (std::any&)v);
                    lua_settable(l, -3);
                }
            }
        }

        void push_veca(lua_State* l, std::any& val) {
            lua_newtable(l);
            if (!std::any_cast<std::vector<std::any>>(val).empty()) {
                int n = 1;
                for (auto&& v : std::any_cast<std::vector<std::any>>(val)) {
                    lua_pushinteger(l, n++);
                    lua::typecast::any(l, (std::any&)v);
                    lua_settable(l, -3);
                }
            }
        }

        void push_vecs(lua_State* l, std::any& val) {
            lua_newtable(l);
            if (!std::any_cast<std::vector<std::string>>(val).empty()) {
                int n = 1;
                for (auto&& v : std::any_cast<std::vector<std::string>>(val)) {
                    lua_pushinteger(l, n++);
                    lua_pushlstring(l, v.c_str(), v.length());
                    lua_settable(l, -3);
                }
            }
        }

        void push_listtt(lua_State* l, std::any& val) {
            lua_newtable(l);
            if (!std::any_cast<std::list<std::pair<std::time_t, std::time_t>>>(val).empty()) {
                int n = 1;
                for (auto&& v : std::any_cast<std::list<std::pair<std::time_t, std::time_t>>>(val)) {
                    lua_pushinteger(l, n++);
                    {
                        lua_newtable(l);
                        lua_pushinteger(l, 1);
                        lua_pushinteger(l, v.first);
                        lua_settable(l, -3);
                        lua_pushinteger(l, 2);
                        lua_pushinteger(l, v.second);
                        lua_settable(l, -3);
                    }
                    lua_settable(l, -3);
                }
            }
        }
        void push_deqmapa(lua_State* l, std::any& val) {

            lua_newtable(l);
            if (!std::any_cast<std::deque<std::unordered_map<std::string, std::any>>>(val).empty()) {
                int n = 1;
                for (auto&& v : std::any_cast<std::deque<std::unordered_map<std::string, std::any>>>(val)) {
                    lua_pushinteger(l, n++);
                    lua_newtable(l);
                    for (auto&& [k, i] : v) {
                        lua_pushlstring(l, k.c_str(), k.length());
                        lua::typecast::any(l, (std::any&)i);
                        lua_settable(l, -3);
                    }
                    lua_settable(l, -3);
                }
            }
        }

        static const std::unordered_map<size_t, std::function<void(lua_State* l, std::any& val)>> PushTypecast{
            {typeid(float).hash_code(),push_number<float>},
            {typeid(double).hash_code(),push_number<double>},
            {typeid(int).hash_code(),push_integer<int>},
            {typeid(ssize_t).hash_code(),push_integer<ssize_t>},
            {typeid(std::time_t).hash_code(),push_integer<std::time_t>},
            {typeid(size_t).hash_code(),push_integer<size_t>},
            {typeid(int64_t).hash_code(),push_integer<int64_t>},
            {typeid(uint64_t).hash_code(),push_integer<uint64_t>},
            {typeid(bool).hash_code(),push_bool},
            {typeid(const char*).hash_code(),push_string},
            {typeid(std::string).hash_code(),push_cstring},
            {typeid(std::vector<uint8_t>).hash_code(),push_rawdata},
            {typeid(std::unordered_map<std::string,std::string>).hash_code(),push_umapss},
            {typeid(std::map<std::string,std::string>).hash_code(),push_mapss},
            {typeid(std::unordered_map<std::string,std::any>).hash_code(),push_mapsa},
            {typeid(std::map<std::string,std::unordered_map<std::string,std::any>>).hash_code(),push_mapssa},
            {typeid(std::set<std::string>).hash_code(),push_sets},
            {typeid(std::list<std::pair<std::time_t, std::time_t>>).hash_code(),push_listtt},
            {typeid(std::set<uint64_t>).hash_code(),push_seti<uint64_t>},
            {typeid(std::deque<std::any>).hash_code(),push_deqa},
            {typeid(std::vector<std::any>).hash_code(),push_veca},
            {typeid(std::vector<std::string>).hash_code(),push_vecs},
            {typeid(std::deque<std::unordered_map<std::string, std::any>>).hash_code(),push_deqmapa},
            {typeid(std::map<std::string,std::any>).hash_code(),push_smapsa},

            {typeid(nullptr).hash_code(),push_nil},
        };

        void any(lua_State* l, std::any& val) {
            if (auto&& it = lua::typecast::PushTypecast.find(val.type().hash_code()); it != lua::typecast::PushTypecast.end()) {
                it->second(l, val);
            }
            else {
                fprintf(stderr, "clua::typecast(%s) no found\n", val.type().name());
            }
        }
    }
}
