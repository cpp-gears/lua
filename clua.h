#pragma once
#include <string>
#include <functional>
#include <any>
#include <unordered_map>
#include <vector>
#include <memory>

extern "C" {
#include <lua5.3/lua.h>
#include <lua5.3/lauxlib.h>
#include <lua5.3/lualib.h>
}

namespace lib {

	class clua {
	public:
		class cargs {
			friend class clua;
			lua_State* luaState{ nullptr };
			ssize_t luaStackOffset{ 1 };
			cargs(lua_State* l, ssize_t o = 1) : luaState(l), luaStackOffset{ o } {; }
		public:
			void get(const std::string& type, ...) const;
		};
		using function = std::function<std::vector<std::any>(const clua::cargs&)>;

		clua(const std::string& scriptname, const std::string& progdir = {});
		~clua();

		void module(const std::string& modname, std::unordered_map<std::string, function>);

		template<typename T>
		void module(const std::string& modname, std::weak_ptr<T> obj, const std::vector<std::pair<std::string, std::vector<std::any>(T::*)(const clua::cargs&)>>& functions) {
			auto&& it = luaModules.emplace(modname, module_t{ .name = modname });

			it.first->second.cfunctions.clear();
			it.first->second.cfunctions.reserve(functions.size() + 1);

			for (auto [name, fn] : functions) {
				auto&& mit = luaMethods.emplace(name, [obj, fn](const clua::cargs& args)->std::vector <std::any>{
					if (auto&& ptr{obj.lock()}; ptr) { return (ptr.get()->*fn)(args); }
					return {};
				});
				it.first->second.cfunctions.push_back(cfunction{ mit.first->first.c_str(),clua::call });
			}
			it.first->second.cfunctions.push_back({ nullptr, nullptr });
		}

		ssize_t execute(const std::string& fn, const std::vector<std::any>&& args = {}, int nresults = 0, std::vector<std::any>&& results = {});

		inline const std::string& pwd() { return luaPwd; }
		inline const std::string& script() { return luaScript; }
		const std::string& script(const std::string_view& scriptname, const std::string_view& progdir = {});
		inline ssize_t check() { if (!luaScript.empty()) { if (auto h{ fopen(luaScript.c_str(),"rt") }; h) { fclose(h); return 0; } return -errno; } return -ERANGE; }
		inline void pwd(const std::string& path) { luaPwd = path; }
	private:
		using cfunction = struct luaL_Reg;
		struct module_t {
		public:
			std::string name;
			std::vector<clua::cfunction> cfunctions;
		};
		static int call(lua_State*);
		inline int __call(lua_State*, const char* function, const clua::cargs& args);
		inline void __module(lua_State* l, const char* name, std::vector<struct luaL_Reg>& fns);
		std::string luaScript, luaPwd;
		std::unordered_map<std::string, module_t> luaModules;
		std::unordered_map<std::string, clua::function> luaMethods;
	};
}